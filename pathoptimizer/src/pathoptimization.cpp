#include "pathoptimization.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cmath>

PathOptimization::PathOptimization()
{
    this->reset_optimization_params();
    this->reset_optimization_delta_params();
    this->reset_filter_params();

    ceiling_height_ = 3.0f; //This should be read in from either map.xml or a so far nonexistens room parameter file "room.xml"
    sample_size_ = 300;
}

void PathOptimization::reset_optimization_params()
{
    optimization_parameters_.theta = 0.0;
    optimization_parameters_.shift_x = 0.0;
    optimization_parameters_.shift_y = 0.0;
    optimization_parameters_.scale = 1.0;
    optimization_parameters_.alpha = 0.0;
    optimization_parameters_.phi = 0.0;
}

void PathOptimization::reset_optimization_delta_params()
{
    delta_parameters_.theta = 0.001;
    delta_parameters_.shift_x = 0.001;
    delta_parameters_.shift_y = 0.001;
    delta_parameters_.scale = 0.001;
    delta_parameters_.alpha = 0.001;
    delta_parameters_.phi = 0.001;
}

void PathOptimization::reset_filter_params()
{
    filter_parameters_.max_x = 2.5;
    filter_parameters_.min_x = -2.5;
    filter_parameters_.max_y = 2.5;
    filter_parameters_.min_y = -2.5;
    filter_parameters_.max_v = 0.5;
    filter_parameters_.mirror_x = true;
}

bool PathOptimization::load_path(std::string filename, path path_name)
{
    std::ifstream is(filename.c_str());
    if(!is){
        return false;
    }
    else
    {
        if(path_name == reference){
            reference_path_.clear();}
        else if (path_name == tracked){
            tracked_path_.clear();}

        std::string line;

        while(getline(is, line))
        {
            PathPoint point;
            std::stringstream ss(line);
            ss >> point.time;
            ss >> point.pos[0];
            ss >> point.pos[1];
            ss >> point.rot;
            if(path_name == reference){
                reference_path_.push_back(point);}
            else if (path_name == tracked){
                tracked_path_.push_back(point);}
        }

        this->calculate_filtered();
        this->calculate_optimized();
        return true;
    }

}

void PathOptimization::print_path(path path_name)
{
    int path_size;
    std::vector<PathPoint> output_path;

    if(path_name == reference){
        path_size = reference_path_.size();
        output_path = reference_path_;}
    else if (path_name == tracked){
        path_size = tracked_path_.size();
        output_path = tracked_path_;}

    for(unsigned i = 0; i < path_size; i++)
    {
        std::cout << output_path[i].time << " " << output_path[i].pos[0] << " " << output_path[i].pos[1] << " " << output_path[i].rot << std::endl;
    }
}

void PathOptimization::calculate_optimized()
{
    int path_size = tracked_path_.size();
    optimized_path_.clear();

    for(unsigned i = 0; i < path_size; i++)
    {
        PathPoint new_optimized_point;
        PathPoint original_point = tracked_path_[i];

        float tilt_shift_distance = ceiling_height_ * tan(optimization_parameters_.alpha);

        //apply rotation, scale and shift
        new_optimized_point.rot = original_point.rot + optimization_parameters_.theta;
        new_optimized_point.pos[0] = ( original_point.pos[0] * std::cos(optimization_parameters_.theta) * optimization_parameters_.scale )
                                  - ( original_point.pos[1] * std::sin(optimization_parameters_.theta) * optimization_parameters_.scale )
                                  + ( optimization_parameters_.shift_x )
                                  + ( tilt_shift_distance * cos(new_optimized_point.rot + optimization_parameters_.phi) );
        new_optimized_point.pos[1] = ( original_point.pos[0] * std::sin(optimization_parameters_.theta) * optimization_parameters_.scale )
                                  + ( original_point.pos[1] * std::cos(optimization_parameters_.theta) * optimization_parameters_.scale )
                                  + ( optimization_parameters_.shift_y )
                                  + ( tilt_shift_distance * sin(new_optimized_point.rot + optimization_parameters_.phi) );


        optimized_path_.push_back(new_optimized_point);
    }

}

void PathOptimization::calculate_filtered()
{
    int ref_path_size = reference_path_.size();
    filtered_reference_path_.clear();
    PathPoint new_filtered_point;
    PathPoint current_filtered_point;
    PathPoint previous_filtered_point;

    std::vector<float> speeds;
    speeds.push_back(0.0);
    for(unsigned i = 1; i < ref_path_size; i++)
    {
        current_filtered_point = reference_path_[i];
        previous_filtered_point = reference_path_[i-1];
        speeds.push_back((current_filtered_point.pos - previous_filtered_point.pos).norm() / (current_filtered_point.time - previous_filtered_point.time));
    }

    //filter out-of-range entries
    for(unsigned i = 0; i < ref_path_size; i++)
    {
        new_filtered_point = reference_path_[i];

        if( new_filtered_point.pos[0] < filter_parameters_.max_x &&
            new_filtered_point.pos[0] > filter_parameters_.min_x &&
            new_filtered_point.pos[1] < filter_parameters_.max_y &&
            new_filtered_point.pos[1] > filter_parameters_.min_y &&
            speeds[i] < filter_parameters_.max_v
          )
        {
            if(filter_parameters_.mirror_x)
            {
                new_filtered_point.pos[1] *= -1.0f;
            }
            filtered_reference_path_.push_back(new_filtered_point);
        }
    }
}

void PathOptimization::downsample(path path_name)
{
    switch(path_name)
    {
    case reference:
        std::random_shuffle(reference_path_.begin(), reference_path_.end());
        reference_path_.erase(reference_path_.begin()+sample_size_, reference_path_.end());
        break;
    case filtered:
        std::random_shuffle(filtered_reference_path_.begin(), filtered_reference_path_.end());
        filtered_reference_path_.erase(filtered_reference_path_.begin()+sample_size_, filtered_reference_path_.end());
    case tracked:
        std::random_shuffle(tracked_path_.begin(), tracked_path_.end());
        tracked_path_.erase(tracked_path_.begin()+sample_size_, tracked_path_.end());
    case optimized:
        std::random_shuffle(optimized_path_.begin(), optimized_path_.end());
        optimized_path_.erase(optimized_path_.begin()+sample_size_, optimized_path_.end());
//    default:
//        PathPoint defaultpoint;
//        return(defaultpoint); break;

    }
}

float PathOptimization::sum_of_quadratic_errors()
{
    //this->downsample(filtered);
    //this->downsample(optimized);
    float sum_of_squared_distances = 0.0;

    // for(const auto& x1 : optimized_path_) //umbauen
    //      x1.pos

    int steps_tracked = optimized_path_.size() / sample_size_;
    int steps_reference = filtered_reference_path_.size() / sample_size_;

    for(unsigned i_t = 0; i_t < optimized_path_.size(); i_t+=steps_tracked)
    {
        float min_squared_distance = (optimized_path_[i_t].pos -filtered_reference_path_[0].pos).squaredNorm();
        for(unsigned i_r = 0; i_r < filtered_reference_path_.size(); i_r+=steps_reference)
        {
            if((optimized_path_[i_t].pos - filtered_reference_path_[i_r].pos).squaredNorm() < min_squared_distance)
            {
                min_squared_distance = (optimized_path_[i_t].pos - filtered_reference_path_[i_r].pos).squaredNorm();
            }
        }
        sum_of_squared_distances += min_squared_distance;
    }


    return(sum_of_squared_distances);
}

void PathOptimization::run_optimization(float rate, int steps)
{
    for(unsigned s = 0; s < steps; s++)
    {
        OptimizationParameters optimization_parameters_current = optimization_parameters_;
        this->calculate_optimized();
        float current_error = this->sum_of_quadratic_errors();

        optimization_parameters_.theta = optimization_parameters_current.theta + delta_parameters_.theta;
        this->calculate_optimized();
        float error_positive = this->sum_of_quadratic_errors();

        optimization_parameters_.theta = optimization_parameters_current.theta - delta_parameters_.theta;
        this->calculate_optimized();
        float error_negative = this->sum_of_quadratic_errors();
        if(error_positive < error_negative)
        {
            optimization_parameters_.theta = optimization_parameters_current.theta + delta_parameters_.theta;
        }
        else
        {
            optimization_parameters_.theta = optimization_parameters_current.theta - delta_parameters_.theta;
        }
        //std::cout << "Delta Error after Step " << s << " is +dir " << error_positive - current_error << " and in -dir "<<  error_negative - current_error << " " << std::endl;
    }
}


PathPoint PathOptimization::get_path_point(path path_name, int index)
{
    switch(path_name)
    {
    case reference:
        return(reference_path_[index]); break;
    case filtered:
        return(filtered_reference_path_[index]); break;
    case tracked:
        return(tracked_path_[index]);   break;
    case optimized:
        return(optimized_path_[index]); break;
//    default:
//        PathPoint defaultpoint;
//        return(defaultpoint); break;

    }
}

int PathOptimization::get_size(path path_name)
{
    if(path_name == reference){
        return(reference_path_.size());}
    else if (path_name == filtered){
        return(filtered_reference_path_.size());}
    else if (path_name == tracked){
        return(tracked_path_.size());}
    else if (path_name == optimized){
        return(optimized_path_.size());}
    else{ return(0);}
}
