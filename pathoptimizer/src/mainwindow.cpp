#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <sstream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    path_optimization_(new PathOptimization)
{
    ui->setupUi(this);

    draw_reference_path_ = false;
    draw_filtered_path_ = true;
    draw_tracked_path_ = false;
    draw_optimized_path_ = true;

    draw_scaling_ = 100;

    if(path_optimization_->load_path("path.txt", reference))
    {
        ui->label_ReferencePathStatus->setText("loaded");
    }
    else
    {
        ui->label_ReferencePathStatus->setText("no path");
    }

    if(path_optimization_->load_path("tracked_path.txt", tracked))
    {
        ui->label_TrackedPathStatus->setText("loaded");
    }
    else
    {
        ui->label_TrackedPathStatus->setText("no path");
    }

    FilterParameters filter_params = path_optimization_->get_filter_parameters();

    ui->doubleSpinBox_minX->setValue(filter_params.min_x);
    ui->doubleSpinBox_maxX->setValue(filter_params.max_x);
    ui->doubleSpinBox_minY->setValue(filter_params.min_y);
    ui->doubleSpinBox_maxY->setValue(filter_params.max_y);
    ui->doubleSpinBox_maxV->setValue(filter_params.max_v);
    ui->checkBox_mirrorX->setChecked(filter_params.mirror_x);


    this->draw();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::draw()
{
    int width = ui->label_drawing->geometry().width();
    int height = ui->label_drawing->geometry().height();
    int centerX = width/2;
    int centerY = height/2;

    QPixmap pixmap(width, height);
    pixmap.fill(QColor("white"));

    QPainter painter(&pixmap);
    QPen pen(Qt::lightGray, 1, Qt::SolidLine);

    painter.setPen(pen);
    painter.drawLine(centerX, 0, centerX, height);
    painter.drawLine(0, centerY, width, centerY);

    for(unsigned x = 0; x < centerX; x+=draw_scaling_)
    {
        painter.drawLine(centerX + x, centerY - 5, centerX + x, centerY + 5);
        painter.drawLine(centerX - x, centerY - 5, centerX - x, centerY + 5);
    }

    for(unsigned y = 0; y < centerY; y+=draw_scaling_)
    {
        painter.drawLine(centerX - 5, centerY + y, centerX + 5, centerY + y);
        painter.drawLine(centerX - 5, centerY - y, centerX + 5, centerY - y);
    }

    pen.setWidth(2);
    pen.setCapStyle(Qt::RoundCap);

    if(draw_reference_path_)
    {
        pen.setColor(QColor(255, 0, 0, 180));
        painter.setPen(pen);
        for(int i=0;i<(path_optimization_->get_size(reference)-1);i++)
            {
                PathPoint current_point = path_optimization_->get_path_point(reference, i);
                PathPoint next_point = path_optimization_->get_path_point(reference, i+1);
                painter.drawLine(draw_scaling_*current_point.pos[0] + centerX,draw_scaling_*current_point.pos[1] + centerY, draw_scaling_*next_point.pos[0] + centerX,draw_scaling_*next_point.pos[1] + centerY);
            }
     }
    if(draw_filtered_path_)
    {
        pen.setColor(QColor(180, 0, 180, 180));
        painter.setPen(pen);
        for(int i=0;i<(path_optimization_->get_size(filtered)-1);i++)
            {
                PathPoint current_point = path_optimization_->get_path_point(filtered, i);
                PathPoint next_point = path_optimization_->get_path_point(filtered, i+1);
                painter.drawLine(draw_scaling_ * current_point.pos[0] + centerX,draw_scaling_ * current_point.pos[1] + centerY, draw_scaling_ * next_point.pos[0] + centerX,draw_scaling_ * next_point.pos[1] + centerY);
            }

    }

    if(draw_tracked_path_)
    {
        pen.setColor(QColor(0, 255, 0, 180));
        painter.setPen(pen);
        for(int i=0;i<(path_optimization_->get_size(tracked)-1);i++)
            {
                PathPoint current_point = path_optimization_->get_path_point(tracked, i);
                PathPoint next_point = path_optimization_->get_path_point(tracked, i+1);
                painter.drawLine(draw_scaling_ * current_point.pos[0] + centerX,draw_scaling_ * current_point.pos[1] + centerY, draw_scaling_ * next_point.pos[0] + centerX,draw_scaling_ * next_point.pos[1] + centerY);
            }
     }
    if(draw_optimized_path_)
    {
        pen.setColor(QColor(0, 180, 180, 180));
        painter.setPen(pen);
        for(int i=0;i<(path_optimization_->get_size(optimized)-1);i++)
            {
                PathPoint current_point = path_optimization_->get_path_point(optimized, i);
                PathPoint next_point = path_optimization_->get_path_point(optimized, i+1);
                painter.drawLine(draw_scaling_ * current_point.pos[0] + centerX,draw_scaling_ * current_point.pos[1] + centerY, draw_scaling_ * next_point.pos[0] + centerX,draw_scaling_ * next_point.pos[1] + centerY);
            }
     }

    ui->label_drawing->setPixmap(pixmap);

}

void MainWindow::path_update()
{
    path_optimization_->calculate_optimized();
    path_optimization_->calculate_filtered();
    this->draw();
}

void MainWindow::on_doubleSpinBox_theta_valueChanged(double arg1)
{
    OptimizationParameters new_params = path_optimization_->get_optimization_parameters();
    new_params.theta=arg1;
    path_optimization_->set_optimization_parameters(new_params);
    this->path_update();
}

void MainWindow::on_doubleSpinBox_shiftX_valueChanged(double arg1)
{
    OptimizationParameters new_params = path_optimization_->get_optimization_parameters();
    new_params.shift_x=arg1;
    path_optimization_->set_optimization_parameters(new_params);
    this->path_update();
}

void MainWindow::on_doubleSpinBox_shiftY_valueChanged(double arg1)
{
    OptimizationParameters new_params = path_optimization_->get_optimization_parameters();
    new_params.shift_y=arg1;
    path_optimization_->set_optimization_parameters(new_params);
    this->path_update();
}

void MainWindow::on_doubleSpinBox_scale_valueChanged(double arg1)
{
    OptimizationParameters new_params = path_optimization_->get_optimization_parameters();
    new_params.scale=arg1;
    path_optimization_->set_optimization_parameters(new_params);
    this->path_update();
}

void MainWindow::on_doubleSpinBox_alpha_valueChanged(double arg1)
{
    OptimizationParameters new_params = path_optimization_->get_optimization_parameters();
    new_params.alpha=arg1;
    path_optimization_->set_optimization_parameters(new_params);
    this->path_update();
}

void MainWindow::on_doubleSpinBox_phi_valueChanged(double arg1)
{
    OptimizationParameters new_params = path_optimization_->get_optimization_parameters();
    new_params.phi=arg1;
    path_optimization_->set_optimization_parameters(new_params);
    this->path_update();
}

void MainWindow::on_pushButton_resetOptimizationParameters_clicked()
{
    path_optimization_->reset_optimization_params();
    ui->doubleSpinBox_theta->setValue(0.0);
    ui->doubleSpinBox_shiftX->setValue(0.0);
    ui->doubleSpinBox_shiftY->setValue(0.0);
    ui->doubleSpinBox_scale->setValue(1.0);
    ui->doubleSpinBox_alpha->setValue(0.0);
    ui->doubleSpinBox_phi->setValue(0.0);
    this->path_update();
}

void MainWindow::on_radioButton_scaleResolution_toggled(bool checked)
{
    if(checked)
    {
        ui->doubleSpinBox_scaleDrawing->setEnabled(true);
        draw_scaling_ = 1.0f / ui->doubleSpinBox_scaleDrawing->value();
        this->draw();
    }
    else
    {
        ui->doubleSpinBox_scaleDrawing->setDisabled(true);
    }

}

void MainWindow::on_doubleSpinBox_scaleDrawing_valueChanged(double arg1)
{
        draw_scaling_ = 1.0f / ui->doubleSpinBox_scaleDrawing->value();
        this->draw();
}

void MainWindow::on_checkBox_drawReference_toggled(bool checked)
{
    draw_reference_path_ = checked;
    this->draw();
}

void MainWindow::on_checkBox_drawFiltered_toggled(bool checked)
{
    draw_filtered_path_ = checked;
    this->draw();
}

void MainWindow::on_checkBox_drawTracked_toggled(bool checked)
{
    draw_tracked_path_ = checked;
    this->draw();
}

void MainWindow::on_checkBox_drawOptimized_toggled(bool checked)
{
    draw_optimized_path_ = checked;
    this->draw();
}

void MainWindow::on_radioButton_scaleBestFit_toggled(bool checked)
{
    best_fit_ = checked;
}

void MainWindow::on_pushButton_calculateError_clicked()
{
    float error = path_optimization_->sum_of_quadratic_errors();

    QString error_string;
    error_string.sprintf("%.3f m^2", error);
    ui->label_error->setText(error_string);
}

void MainWindow::on_checkBox_mirrorX_toggled(bool checked)
{
    FilterParameters new_params = path_optimization_->get_filter_parameters();
    new_params.mirror_x = checked;
    path_optimization_->set_filter_parameters(new_params);
    this->path_update();
}

void MainWindow::on_doubleSpinBox_minX_valueChanged(double arg1)
{
    FilterParameters new_params = path_optimization_->get_filter_parameters();
    new_params.min_x = arg1;
    path_optimization_->set_filter_parameters(new_params);
    this->path_update();
}

void MainWindow::on_doubleSpinBox_maxX_valueChanged(double arg1)
{
    FilterParameters new_params = path_optimization_->get_filter_parameters();
    new_params.max_x = arg1;
    path_optimization_->set_filter_parameters(new_params);
    this->path_update();
}

void MainWindow::on_doubleSpinBox_minY_valueChanged(double arg1)
{
    FilterParameters new_params = path_optimization_->get_filter_parameters();
    new_params.min_y = arg1;
    path_optimization_->set_filter_parameters(new_params);
    this->path_update();
}

void MainWindow::on_doubleSpinBox_maxY_valueChanged(double arg1)
{
    FilterParameters new_params = path_optimization_->get_filter_parameters();
    new_params.max_y = arg1;
    path_optimization_->set_filter_parameters(new_params);
    this->path_update();
}

void MainWindow::on_doubleSpinBox_maxV_valueChanged(double arg1)
{
    FilterParameters new_params = path_optimization_->get_filter_parameters();
    new_params.max_v = arg1;
    path_optimization_->set_filter_parameters(new_params);
    this->path_update();
}

void MainWindow::on_pushButton_resetPreprocessing_clicked()
{
    path_optimization_->reset_filter_params();
    FilterParameters filter_params = path_optimization_->get_filter_parameters();

    ui->doubleSpinBox_minX->setValue(filter_params.min_x);
    ui->doubleSpinBox_maxX->setValue(filter_params.max_x);
    ui->doubleSpinBox_minY->setValue(filter_params.min_y);
    ui->doubleSpinBox_maxY->setValue(filter_params.max_y);
    ui->doubleSpinBox_maxV->setValue(filter_params.max_v);
    ui->checkBox_mirrorX->setChecked(filter_params.mirror_x);

    this->path_update();
}

void MainWindow::on_pushButton_runOptimization_clicked()
{
    path_optimization_->run_optimization(ui->doubleSpinBox_rate->value(), ui->spinBox_steps->value());

    ui->doubleSpinBox_theta->setValue(path_optimization_->get_optimization_parameters().theta);

    float error = path_optimization_->sum_of_quadratic_errors();
    QString error_string;
    error_string.sprintf("%.3f m^2", error);
    ui->label_error->setText(error_string);

    this->draw();

}
