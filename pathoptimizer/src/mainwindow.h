#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QColor>
#include <pathoptimization.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    void draw();
    void path_update();
    ~MainWindow();

private Q_SLOTS:

    void on_doubleSpinBox_theta_valueChanged(double arg1);

    void on_doubleSpinBox_shiftX_valueChanged(double arg1);

    void on_doubleSpinBox_shiftY_valueChanged(double arg1);

    void on_doubleSpinBox_scale_valueChanged(double arg1);

    void on_doubleSpinBox_alpha_valueChanged(double arg1);

    void on_doubleSpinBox_phi_valueChanged(double arg1);

    void on_pushButton_resetOptimizationParameters_clicked();

    void on_radioButton_scaleResolution_toggled(bool checked);

    void on_doubleSpinBox_scaleDrawing_valueChanged(double arg1);

    void on_checkBox_drawFiltered_toggled(bool checked);

    void on_checkBox_drawReference_toggled(bool checked);

    void on_checkBox_drawTracked_toggled(bool checked);

    void on_checkBox_drawOptimized_toggled(bool checked);

    void on_radioButton_scaleBestFit_toggled(bool checked);

    void on_pushButton_calculateError_clicked();

    void on_checkBox_mirrorX_toggled(bool checked);

    void on_doubleSpinBox_minX_valueChanged(double arg1);

    void on_doubleSpinBox_maxX_valueChanged(double arg1);

    void on_doubleSpinBox_minY_valueChanged(double arg1);

    void on_doubleSpinBox_maxY_valueChanged(double arg1);

    void on_doubleSpinBox_maxV_valueChanged(double arg1);

    void on_pushButton_resetPreprocessing_clicked();

    void on_pushButton_runOptimization_clicked();

private:
    Ui::MainWindow *ui;
    PathOptimization *path_optimization_;
    bool draw_reference_path_;
    bool draw_filtered_path_;

    bool draw_tracked_path_;
    bool draw_optimized_path_;

    float draw_scaling_;
    bool best_fit_;

};

#endif // MAINWINDOW_H
