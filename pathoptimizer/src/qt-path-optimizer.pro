#-------------------------------------------------
#
# Project created by QtCreator 2013-02-04T17:27:10
#
#-------------------------------------------------

QT       += core gui

TARGET = qt-path-optimizer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    pathoptimization.cpp

HEADERS  += mainwindow.h \
    pathoptimization.h

FORMS    += mainwindow.ui
