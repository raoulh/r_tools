#ifndef PATHOPTIMIZATION_H
#define PATHOPTIMIZATION_H

#include <vector>
#include <string>
#include <Eigen/Dense>

struct PathPoint
{
    float time;
    Eigen::Matrix<float, 2, 1> pos;
    float rot;
};

struct FilterParameters
{
    float max_x;
    float min_x;
    float max_y;
    float min_y;
    float max_v;
    bool mirror_x;
};

struct OptimizationParameters
{
    float theta;    // rotation around 0.0, 0.0
    float shift_x;  // shift added in x-direction
    float shift_y;  // shift added in y-direction
    float scale;    // scaling aroud 0.0, 0.0
    float alpha;    // camera tilt direction
    float phi;      // angle of camera tilt
};

enum path {reference, filtered, tracked, optimized};

class PathOptimization
{
public:
    PathOptimization();

    void reset_optimization_params();
    void reset_optimization_delta_params();
    void reset_filter_params();
    bool load_path(std::string filename, path path_name);
    void print_path(path path_name);

    void calculate_optimized();
    void calculate_filtered();

    void downsample(path path_name);
    float sum_of_quadratic_errors();
    void run_optimization(float rate, int steps);

    PathPoint get_path_point(path path_name, int index);
    int get_size(path path_name);

    OptimizationParameters get_optimization_parameters(){return optimization_parameters_;}
    void set_optimization_parameters(OptimizationParameters new_optimization_parameters){optimization_parameters_=new_optimization_parameters;}

    FilterParameters get_filter_parameters(){return filter_parameters_;}
    void set_filter_parameters(FilterParameters new_filter_parameters){filter_parameters_=new_filter_parameters;}

private:
    std::vector<PathPoint> reference_path_;
    std::vector<PathPoint> filtered_reference_path_;

    std::vector<PathPoint> tracked_path_;
    std::vector<PathPoint> optimized_path_;

    OptimizationParameters optimization_parameters_;
    OptimizationParameters delta_parameters_;
    FilterParameters filter_parameters_;

    int sample_size_;

    float ceiling_height_;
};

#endif // PATHOPTIMIZATION_H
